﻿using System;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace HWNotification
{
    public class SQLHelper
    {
        private string mstr_ConnectionString = string.Empty;
    
        private MySqlConnection mobj_SqlConnection;
        private MySqlCommand mobj_SqlCommand;
        private int mint_CommandTimeout = 30;
        //SMSService SS = new SMSService();
        public SQLHelper()
        {
            // SS = new SMSService();
        }

        public void InitializeDataConnecion(string conn)
        {
            try
            {
              
                mobj_SqlConnection = new MySqlConnection(conn);// );con
                mobj_SqlCommand = new MySqlCommand();
                mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
                mobj_SqlCommand.Connection = mobj_SqlConnection;

                //ParseConnectionString();
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex.Message.ToString());
                throw new Exception("Error initializing data class." + Environment.NewLine + ex.Message);
            }
        }


        public void CloseConnection()
        {

            try
            {
                // if (mobj_SqlConnection.State != ConnectionState.Closed) mobj_SqlConnection.Close();
                if (mobj_SqlConnection != null && mobj_SqlConnection.State == ConnectionState.Open)
                {

                    mobj_SqlConnection.Close();
                }
                //Clean Up Command Object
                if (mobj_SqlCommand != null)
                {
                    mobj_SqlCommand.Parameters.Clear();
                    mobj_SqlCommand.Dispose();
                }
            }
            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
            }
        }



        public void AddParameterToSQLCommand(string ParameterName, MySqlDbType ParameterType)
        {
            try
            {
                if (!mobj_SqlCommand.Parameters.Contains(ParameterName))
                    mobj_SqlCommand.Parameters.Add(new MySqlParameter(ParameterName, ParameterType));
                // _logger.LogError("command parameter already exists");
            }
            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
                throw ex;
            }
        }

        public void AddParameterToSQLCommand(string ParameterName, MySqlDbType ParameterType, int ParameterSize)
        {
            try
            {
                mobj_SqlCommand.Parameters.Add(new MySqlParameter(ParameterName, ParameterType, ParameterSize));
            }
            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
                throw ex;
            }
        }

        public void SetSQLCommandParameterValue(string ParameterName, object Value)
        {
            try
            {
                mobj_SqlCommand.Parameters[ParameterName].Value = Value;
            }
            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
                throw ex;
            }
        }


        #region ExecuteNonQuery

        public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {
            //create & open a MySqlConnection, and dispose of it after we are done.
            using (MySqlConnection cn = new MySqlConnection(connectionString))
            {
                cn.Open();
                //call the overload that takes a connection in place of the connection string
                return ExecuteNonQuery(cn, commandType, commandText, commandParameters);
            }
        }

        public static int ExecuteNonQuery(MySqlConnection connection, CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {
            //create a command and prepare it for execution
            MySqlCommand cmd = new MySqlCommand();
            PrepareCommand(cmd, connection, (MySqlTransaction)null, commandType, commandText, commandParameters);

            //finally, execute the command.
            int retval = cmd.ExecuteNonQuery();

            // detach the MySqlParameters from the command object, so they can be used again.
            cmd.Parameters.Clear();
            return retval;
        }

        public static int ExecuteNonQuery(MySqlTransaction transaction, CommandType commandType, string commandText)
        {
            //pass through the call providing null for the set of MySqlParameters
            return ExecuteNonQuery(transaction, commandType, commandText, (MySqlParameter[])null);
        }

        public static int ExecuteNonQuery(MySqlTransaction transaction, CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {
            //create a command and prepare it for execution
            MySqlCommand cmd = new MySqlCommand();
            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters);

            //finally, execute the command.
            int retval = cmd.ExecuteNonQuery();

            // detach the MySqlParameters from the command object, so they can be used again.
            cmd.Parameters.Clear();
            return retval;
        }

        #endregion

        public int UpdateFCMDetails(string ConnectionString, string slno)
        {
            int result = 0;
            try
            {
                result = ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "Update_FCMDetails",
                    new MySqlParameter("@Slno", slno));
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int UpdateFemaleAlertStatus(string ConnectionString, string RouteIds, int status)
        {
            int result = 0;
            try
            {
                result = ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "Update_FemaleAlertStatus",
                    new MySqlParameter("@RouteIds", RouteIds),
                    new MySqlParameter("@Status ", status));
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int UpdateTripDelayAlertStatus(string ConnectionString, string slno)
        {
            int result = 0;
            try
            {
                result = ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "Update_DelayAlertStatus",
                    new MySqlParameter("@Slno", slno));
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int UpdateSMSStatusDetails(string ConnectionString, string slno)
        {
            int result = 0;
            try
            {
                result = ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "UpdateSMSStatus_FCMDetails",
                    new MySqlParameter("@Slno", slno));
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int UpdateSMSResendStatusAndCount(string ConnectionString, string slno)
        {
            int result = 0;
            try
            {
                result = ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "Update_SmsResendStatusAndCount",
                    new MySqlParameter("@Slno", slno));
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public DataSet Get_EmpandDriverDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_EmpandDriverDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_EmpDropDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_EmpDropDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_PickupFCMDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_PickupFCMDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_ResendFCMMessageDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_ResendFCMDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_TripDelayAlertMessageDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "TripDelayDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }
        public DataSet Get_ParentMessageDetails(string ConnectionString, string RFIDTag)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_NotificationFCMDatials",
                    new MySqlParameter("@RfID", RFIDTag));
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_GetUserWiseSettingsForFcm(string ConnectionString, string imei)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_IMEIAlertSettings",
                    new MySqlParameter("@Par_IMEI", imei));
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }


        public DataSet Get_GeofenceNotificationAlerts(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_SendCustomMessage");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_NotificationMessageDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "StudentFCMDetails_Pranay");
                // new MySqlParameter("@Adminid", AdminID));
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet GetimeiDetails(string ConnectionString)
        {

                string userId = "PowerUser";
                int userType = 1;

                DataSet ds = new DataSet();

                try
                {
                    ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_IMEIs",
                        new MySqlParameter("@Par_UserName", userId),
                         new MySqlParameter("@Par_UserType", userType));
                }
                catch (Exception ex)
                {
                    ex = null;
                }
                return ds;
        }


        public DataSet Get_RFIDList(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_RfIDs");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet Get_ParentMessages(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_ParentMessageDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public void Update_NotificationStatus(string ConnectionString, int slno, int updateStatus)
        {
            MySqlConnection sqlConn = null;
            int status = 0;
            MySqlCommand cmd = null;

            try
            {
                using (sqlConn = new MySqlConnection(ConnectionString))
                {
                    cmd = new MySqlCommand("Update_NotificationStatus", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = Convert.ToInt32(200);

                    cmd.Parameters.Add("@Slno", MySqlDbType.VarChar, 20).Value = slno;
                    cmd.Parameters.Add("@Status", MySqlDbType.Int32).Value = updateStatus;

                    sqlConn.Open();
                    status = cmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                }
                ex = null;
            }
        }


        public DataSet Get_DropFCMDetails(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_DropFCMDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public DataSet GetisFirstFemaleData(string ConnectionString)
        {
            DataSet ds = new DataSet();

            try
            {
                ds = ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Get_FemalePickupAndDropDetails");
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return ds;
        }

        public int InsertPickupDetails(string Connectionstring, string EmployeeNo, string EmployeeName, string EmployeeMobileNo, string RouteId, string RoutePointId, string PickupLocation, string PickUptime, string DriverName, string VehicleNo, string DriverMobileNo, string FCMToken, string EmpOTP)
        {
            MySqlConnection sqlConn = null;
            int status = 0;
            MySqlCommand cmd = null;

            try
            {
                using (sqlConn = new MySqlConnection(Connectionstring))
                {
                    cmd = new MySqlCommand("Insert_PickUpDetails", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = Convert.ToInt32(200);

                    cmd.Parameters.Add("@EmployeeNo", MySqlDbType.VarChar, 20).Value = EmployeeNo;
                    cmd.Parameters.Add("@EmployeeName", MySqlDbType.VarChar, 50).Value = EmployeeName;
                    cmd.Parameters.Add("@EmployeeMobileNo", MySqlDbType.VarChar, 50).Value = EmployeeMobileNo;
                    cmd.Parameters.Add("@RouteId", MySqlDbType.Int32).Value = RouteId;
                    cmd.Parameters.Add("@RoutePointId", MySqlDbType.Int32).Value = RoutePointId;
                    cmd.Parameters.Add("@VehicleNo", MySqlDbType.VarChar, 20).Value = VehicleNo;
                    cmd.Parameters.Add("@PickupLocation", MySqlDbType.VarChar, 100).Value = PickupLocation;
                    cmd.Parameters.Add("@PickUptime", MySqlDbType.VarChar, 10).Value = PickUptime;
                    cmd.Parameters.Add("@DriverName", MySqlDbType.VarChar, 50).Value = DriverName;
                    cmd.Parameters.Add("@DriverMobileNo", MySqlDbType.VarChar, 50).Value = DriverMobileNo;
                    cmd.Parameters.Add("@FCMToken", MySqlDbType.VarChar, 2048).Value = FCMToken;
                    cmd.Parameters.Add("@OTP", MySqlDbType.VarChar, 20).Value = EmpOTP;

                    sqlConn.Open();
                    status = cmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                }
                sqlex = null;
            }
            catch (Exception ex)
            {
                // WriteErrorLogMessage("Error in InsertFCMDetails(): " + ex.Message, LogFile);
                ex = null;
            }

            finally
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Dispose();
                    sqlConn = null;
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }
            }
            return status;
        }

        public int InsertDropDetails(string Connectionstring, string EmployeeNo, string EmployeeName, string EmployeeMobileNo, string RouteId, string RoutePointId, string DropLocation, string Droptime, string DriverName, string VehicleNo, string DriverMobileNo, string FCMToken, string EmpDropOTP)
        {
            MySqlConnection sqlConn = null;
            int status = 0;
            MySqlCommand cmd = null;

            try
            {
                using (sqlConn = new MySqlConnection(Connectionstring))
                {
                    cmd = new MySqlCommand("Insert_DropDetails", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = Convert.ToInt32(200);

                    cmd.Parameters.Add("@EmployeeNo", MySqlDbType.VarChar, 20).Value = EmployeeNo;
                    cmd.Parameters.Add("@EmployeeName", MySqlDbType.VarChar, 50).Value = EmployeeName;
                    cmd.Parameters.Add("@EmployeeMobileNo", MySqlDbType.VarChar, 50).Value = EmployeeMobileNo;
                    cmd.Parameters.Add("@RouteId", MySqlDbType.Int32).Value = RouteId;
                    cmd.Parameters.Add("@RoutePointId", MySqlDbType.Int32).Value = RoutePointId;
                    cmd.Parameters.Add("@VehicleNo", MySqlDbType.VarChar, 20).Value = VehicleNo;
                    cmd.Parameters.Add("@DropLocation", MySqlDbType.VarChar, 100).Value = DropLocation;
                    cmd.Parameters.Add("@DropTime", MySqlDbType.VarChar, 10).Value = Droptime;
                    cmd.Parameters.Add("@DriverName", MySqlDbType.VarChar, 50).Value = DriverName;
                    cmd.Parameters.Add("@DriverMobileNo", MySqlDbType.VarChar, 50).Value = DriverMobileNo;
                    cmd.Parameters.Add("@FCMToken", MySqlDbType.VarChar, 2048).Value = FCMToken;
                    cmd.Parameters.Add("@DropOtp", MySqlDbType.VarChar, 20).Value = EmpDropOTP;

                    sqlConn.Open();
                    status = cmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                }
                sqlex = null;
            }
            catch (Exception ex)
            {
                // WriteErrorLogMessage("Error in InsertFCMDetails(): " + ex.Message, LogFile);
                ex = null;
            }
            finally
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Dispose();
                    sqlConn = null;
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }
            }
            return status;
        }

        public DataSet GetDatasetByCommand(string Command)
        {

            MySqlDataAdapter adpt = null;
            DataSet ds = new DataSet();
            try
            {
                mobj_SqlCommand.CommandText = Command;
                mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
                mobj_SqlCommand.CommandType = CommandType.StoredProcedure;
                try
                {
                    if (mobj_SqlConnection.State == ConnectionState.Closed)
                        mobj_SqlConnection.Open();
                    adpt = new MySqlDataAdapter(mobj_SqlCommand);

                    adpt.Fill(ds);
                    // close the connection
                    CloseConnection();
                    // dispose the adapter
                    adpt.Dispose();
                    //return ds;
                }
                catch (Exception ex)
                {
                    //_logger.LogError(ex.Message.ToString());
                    CloseConnection();
                    throw ex;
                }
                return ds;
            }

            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
                CloseConnection();

                throw ex;
            }
            finally
            {
                CloseConnection();

                if (adpt != null)
                    adpt.Dispose();
            }
            return ds;
        }


        public void CloseConnection(MySqlCommand cmd)
        {

            try
            {
                // if (mobj_SqlConnection.State != ConnectionState.Closed) mobj_SqlConnection.Close();
                if (mobj_SqlConnection != null && mobj_SqlConnection.State == ConnectionState.Open)
                {

                    mobj_SqlConnection.Close();
                }
                //Clean Up Command Object
                if (cmd != null)
                {
                    cmd.Parameters.Clear();
                    cmd.Dispose();
                }
            }
            catch (Exception ex)
            {
                // _logger.LogError(ex.Message.ToString());
            }
        }


        public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
        {
            return ExecuteDataset(connectionString, commandType, commandText, (MySqlParameter[])null);
        }

        public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {

            using (MySqlConnection cn = new MySqlConnection(connectionString))
            {
                cn.Open();

                return ExecuteDataset(cn, commandType, commandText, commandParameters);
            }
        }

        public static DataSet ExecuteDataset(MySqlConnection connection, CommandType commandType, string commandText, params MySqlParameter[] commandParameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            PrepareCommand(cmd, connection, (MySqlTransaction)null, commandType, commandText, commandParameters);

            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            da.Fill(ds);
            cmd.Parameters.Clear();

            return ds;
        }

        private static void PrepareCommand(MySqlCommand command, MySqlConnection connection, MySqlTransaction transaction, CommandType commandType, string commandText, MySqlParameter[] commandParameters)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            command.Connection = connection;
            command.CommandText = commandText;

            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            command.CommandType = commandType;

            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }

            return;
        }

        private static void AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        {
            foreach (MySqlParameter p in commandParameters)
            {
                if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }
                command.Parameters.Add(p);
            }
        }



        internal int Insert_NotificationAlertsStatus(string Connectionstring, string VehicleNo, string StudentID, string RouteNo, DateTime Trackdatetime, string BusStop, string Status, string GeofenceName, bool DoNotDiturb, string Latitude, string Longitude, sbyte SendStatus, string Location, string MobileNo)
        {
            MySqlConnection sqlConn = null;
            int status = 0;
            MySqlCommand cmd = null;

            try
            {
                using (sqlConn = new MySqlConnection(Connectionstring))
                {
                    cmd = new MySqlCommand("Insert_NotificationSendStatusDetails", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = Convert.ToInt32(200);

                    cmd.Parameters.Add("@AssetNo", MySqlDbType.VarChar, 20).Value = VehicleNo;
                    cmd.Parameters.Add("@StudentID", MySqlDbType.VarChar, 50).Value = StudentID;
                    cmd.Parameters.Add("@RouteNo", MySqlDbType.VarChar, 50).Value = RouteNo;
                    cmd.Parameters.Add("@TrackDateTime", MySqlDbType.DateTime).Value = Trackdatetime;
                    cmd.Parameters.Add("@StopPoint", MySqlDbType.VarChar, 50).Value = BusStop;
                    cmd.Parameters.Add("@Status", MySqlDbType.VarChar, 20).Value = Status;
                    cmd.Parameters.Add("@GeofenceName", MySqlDbType.VarChar, 100).Value = GeofenceName;
                    cmd.Parameters.Add("@DoNotDiturb", MySqlDbType.Bit).Value = DoNotDiturb;
                    cmd.Parameters.Add("@Latitude", MySqlDbType.VarChar,10).Value = Latitude;
                    cmd.Parameters.Add("@Longitude", MySqlDbType.VarChar,10).Value = Longitude;
                    cmd.Parameters.Add("@SendStatus", MySqlDbType.Int32).Value = SendStatus;
                    cmd.Parameters.Add("@Location", MySqlDbType.VarChar,300).Value = Location;
                    cmd.Parameters.Add("@MobileNo", MySqlDbType.VarChar, 50).Value = MobileNo;

                    sqlConn.Open();
                    status = cmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (SqlException sqlex)
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                }
                sqlex = null;
            }
            catch (Exception ex)
            {
                // WriteErrorLogMessage("Error in InsertFCMDetails(): " + ex.Message, LogFile);
                ex = null;
            }
            finally
            {
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Dispose();
                    sqlConn = null;
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }
            }
            return status;
        }
    }
}
