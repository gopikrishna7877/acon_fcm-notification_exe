﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetConnNotifications
{
  
    public class Model
    {
        public string imei { get; set; }
        public string fcmtoken { get; set; }
    }

    public class AlertData
    {
        public string AssetNo { get; set; }
        public string IMEI { get; set; }
        public string TimeStamp { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string AlertCode { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string AssetType { get; set; }
        public string SendStatus { get; set; }
        public string Loc { get; set; }
    }

    public class FcmDetails
    {
        public string IMEI { get; set; }
        public string FcmToken { get; set; }
        public string IsBlocked { get; set; }
        public string SubscribeStatus { get; set; }
        public string Source { get; set; }
        public string UserId { get; set; }
    }

    public class ImeiListFcmToken
    {
        public string imei { get; set; }
        public object fcminfo { get; set; }
    }

}
