﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Linq;
using System.Data.SqlClient;
using Newtonsoft.Json;
using AssetConnNotifications;
using System.Collections.Generic;
using System.Messaging;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System.Globalization;
using System.ComponentModel;
using System.Threading;
using System.Collections;
using MySql.Data.MySqlClient;

namespace HWNotification
{
    public partial class Notification : Form
    {

        int TimerInterval;
        string LogFile = string.Empty;
        SQLHelper SH = null;

        string Connectionstring = string.Empty;
        string appKey = string.Empty;
        string senderId = string.Empty;
        string HttpsUrl = string.Empty;
        string Subject = string.Empty;

        public string C_ServerIp = string.Empty;
        public int C_Port;
        public string C_KeySpace = string.Empty;
        public string C_UserName = string.Empty;
        public string C_Password = string.Empty;

        MessageQueue messageQueue = null;
        string CasQName = string.Empty;
        IMongoDatabase database = null;


        public Notification()
        {
            InitializeComponent();

            SH = new SQLHelper();

            TimerInterval = Convert.ToInt32(ConfigurationManager.AppSettings["Interval"]);
            LogFile = Convert.ToString(ConfigurationManager.AppSettings["LogFile"]);

            Connectionstring = Convert.ToString(ConfigurationManager.AppSettings["Connectionstring"]);

            appKey = Convert.ToString(ConfigurationManager.AppSettings["appKey"]);
            senderId = Convert.ToString(ConfigurationManager.AppSettings["senderId"]);
        }

        private void Notification_Load(object sender, EventArgs e)
        {
            //Timer_initialization();
            WriteLogMessage("Application Started.....", LogFile);

            ReadMessagesFromQueue();
        }

        public void ReadMessagesFromQueue()
        {
            CasQName = System.Configuration.ConfigurationManager.AppSettings["CasandraQName"].ToString();
            string queuePath = @".\Private$\" + CasQName;


            try
            {
                CasQName = System.Configuration.ConfigurationManager.AppSettings["CasandraQName"].ToString();
                string path = @".\Private$\" + CasQName;

                //string path = @".\Private$\" + "casjvt6613";

                if (!MessageQueue.Exists(path))
                {
                    messageQueue = MessageQueue.Create(path);
                }
                else
                {
                    messageQueue = new MessageQueue(path);
                }

                //messageQueue.Formatter = new BinaryMessageFormatter(); //localsyastem
                messageQueue.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) }); //In server
                messageQueue.ReceiveCompleted += OnReceiveCompleted;
                messageQueue.BeginReceive();
            }
            catch (Exception ex)
            {
                WriteErrorLogMessage("Error in Connecting MSMQ....", LogFile);
            }

        }

        private async void OnReceiveCompleted(Object source, ReceiveCompletedEventArgs asyncResult)
        {

            MessageQueue mq = (MessageQueue)source;

            if (mq != null)
            {
                try
                {
                    System.Messaging.Message message = null;

                    message = mq.EndReceive(asyncResult.AsyncResult);

                    if (message != null)
                    {
                        string data = message.Body as string;

                        if (!string.IsNullOrEmpty(data))
                        {
                            await Task.Run(() => GetNotificationAlerts(data));
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteErrorLogMessage("Error in OnReceiveCompleted", LogFile);
                }
                finally
                {
                    mq.BeginReceive();
                }
            }

        }

        private async void GetNotificationAlerts(string data)
        {
            DataSet GNA = new DataSet();
            List<FcmDetails> fcmData = new List<FcmDetails>();
            FcmDetails fcmObj = null;
            string sendNotification = Convert.ToString(ConfigurationManager.AppSettings["SendNotification"]);

            WriteLogMessage("Data from Queue : " + data, LogFile);

            try
            {
                AlertData alertObj = GetAlertObject(data);

                if (database == null)
                    database = GetMongoConnection();

                ImeiListFcmToken TokenData = new ImeiListFcmToken();
                //TrackerDetailsVno details = null;
                FcmDetails info = null;

                IMongoCollection<BsonDocument> _collection = database.GetCollection<BsonDocument>("fcmdetails");

                var condition = Builders<BsonDocument>.Filter;
                var filter = condition.Eq("fcminfo.subscribestatus", 1) & condition.Eq("fcminfo.isblocked", 1);
                var fields = Builders<BsonDocument>.Projection.Include("imei").Include("fcminfo").Exclude("_id");

                var list = _collection.Find(filter).Project(fields).ToList();

                foreach (var item in list)
                {
                    try
                    {
                        TokenData = BsonSerializer.Deserialize<ImeiListFcmToken>(item);
                        info = JsonConvert.DeserializeObject<FcmDetails>(TokenData.fcminfo.ToJson());

                        fcmObj = new FcmDetails();
                        fcmObj.IMEI = item["imei"].ToString();
                        fcmObj.FcmToken = info.FcmToken;
                        fcmObj.IsBlocked = Convert.ToString(info.IsBlocked);
                        fcmObj.SubscribeStatus = Convert.ToString(info.SubscribeStatus);
                        fcmObj.Source = info.Source;
                        fcmObj.UserId = info.UserId;

                        fcmData.Add(fcmObj);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                //using (ISession session = GetCassandraConnection())
                //{

                //RowSet rs = session.Execute("select * from fcmdetails where isblocked=1 and subscribestatus=1 allow filtering");


                //if (rs != null && rs.GetAvailableWithoutFetching() > 0)
                //{
                //    foreach (Row item in rs)
                //    {
                //        try
                //        {
                //            fcmObj = new FcmDetails();
                //            fcmObj.IMEI = item["imei"].ToString();
                //            fcmObj.FcmToken = item["fcmtoken"].ToString();
                //            fcmObj.IsBlocked = item["isblocked"].ToString();
                //            fcmObj.SubscribeStatus = item["subscribestatus"].ToString();
                //            fcmObj.Source = item["source"].ToString();
                //            fcmObj.UserName = item["userid"].ToString();

                //            fcmData.Add(fcmObj);
                //        }
                //        catch (Exception ex)
                //        {

                //        }
                //    }
                //string conn = Convert.ToString(ConfigurationManager.AppSettings["ConnectionString"]);

                SH.InitializeDataConnecion(Connectionstring);


                SH.AddParameterToSQLCommand("@Par_IMEI", MySqlDbType.Int64);
                SH.SetSQLCommandParameterValue("@Par_IMEI", alertObj.IMEI);

                GNA = SH.GetDatasetByCommand("Get_IMEIAlertSettings");

                SH.CloseConnection();

               // GNA = SH.Get_GetUserWiseSettingsForFcm(Connectionstring, alertObj.IMEI);

                if (GNA != null && GNA.Tables.Count > 0)
                {
                    var userList = GNA.Tables[0].AsEnumerable().Where(x => x.Field<string>("StatusCode") == "1").Select(y => new
                    {
                        StatusCode = y.Field<string>("StatusCode"),
                        UserName = y.Field<string>("UserID")

                    }).ToList();

                    List<FcmDetails> finalList = fcmData.Join(userList, x => x.UserId.ToLower(), y => y.UserName.ToLower(), (x, y) => new { fcm = x, ulist = y }).Where(x => x.fcm.UserId.ToLower() == x.ulist.UserName.ToLower()).Select(s => new FcmDetails
                    {
                        IMEI = s.fcm.IMEI,
                        FcmToken = s.fcm.FcmToken,
                        IsBlocked = s.fcm.IsBlocked,
                        SubscribeStatus = s.fcm.SubscribeStatus,
                        Source = s.fcm.Source,
                        UserId = s.fcm.UserId

                    }).ToList();


                    if (sendNotification.ToLower() == "true")
                    {
                        await Task.Run(() => SendFcmNotification(finalList, alertObj));
                    }

                }
                //}

                //}

                #region OLD Code

                //GNA = SH.Get_NotificationMessageDetails(Connectionstring);

                //if (GNA != null)
                //{
                //    if (GNA.Tables.Count > 0 && GNA.Tables[0].Rows.Count > 0)
                //    {
                //        for (int i = 0; i < GNA.Tables[0].Rows.Count; i++)
                //        {
                //            string VehicleNo = GNA.Tables[0].Rows[i]["VehicleNo"].ToString();
                //            string StudentName = GNA.Tables[0].Rows[i]["StudentName"].ToString();
                //            DateTime Trackdatetime = Convert.ToDateTime(GNA.Tables[0].Rows[i]["TrackdateTime"]);
                //            string DeviceNo = GNA.Tables[0].Rows[i]["DeviceNo"].ToString();
                //            string Latitide = GNA.Tables[0].Rows[i]["Latitude"].ToString();
                //            string Longitude = GNA.Tables[0].Rows[i]["Longitude"].ToString();
                //            string FCMToken = GNA.Tables[0].Rows[i]["FCMToken"].ToString();
                //            string Location = GNA.Tables[0].Rows[i]["Location"].ToString();
                //            string AlertFor = GNA.Tables[0].Rows[i]["AlertFor"].ToString();
                //            string RouteNo = GNA.Tables[0].Rows[i]["RouteNo"].ToString();
                //            string StudentID = GNA.Tables[0].Rows[i]["StudentID"].ToString();
                //            string BusStop = GNA.Tables[0].Rows[i]["BusStopID"].ToString();
                //            string GeofenceName = GNA.Tables[0].Rows[i]["GeofenceName"].ToString();
                //            string Status = GNA.Tables[0].Rows[i]["Status"].ToString();
                //            bool DoNotDiturb = Convert.ToBoolean(GNA.Tables[0].Rows[i]["DoNotDisturb"]);
                //            string MobileNo = GNA.Tables[0].Rows[i]["MobileNo"].ToString();

                //            WriteLogMessage("Notification Data Received For :" + VehicleNo, LogFile);

                //            SendNotificationAlerts(StudentName, VehicleNo, Trackdatetime, Location, DeviceNo, FCMToken, AlertFor, Latitide, Longitude, RouteNo, StudentID, BusStop, GeofenceName, Status, DoNotDiturb, MobileNo);

                //            System.Threading.Thread.Sleep(50);

                //        }
                //    }
                //} 

                #endregion

            }
            catch (Exception exe)
            {
                WriteErrorLogMessage("Error in GetNotificationAlerts() : " + exe.Message, LogFile);
                exe = null;
            }
            finally
            {
                GNA.Dispose();
            }
        }

        private string GetAlertName(string alc)
        {
            string alertName = string.Empty;

            if (alc == "2" || alc == "38")
                alertName = "Tamper";
            else if (alc == "3")
                alertName = "Over Speed";
            else if (alc == "5")
                alertName = "SOS";
            else if (alc == "7")
                alertName = "Tow";
            else if (alc == "16")
                alertName = "Ignition ON";
            else if (alc == "33")
                alertName = "Ignition OFF";
            else if (alc == "18")
                alertName = "AC ON";
            else if (alc == "47")
                alertName = "AC OFF";
            else if (alc == "22")
                alertName = "Geofence Entry";
            else if (alc == "23")
                alertName = "Geofence Exit";
            else
                alertName = "Other";

            return alertName;
        }


        public DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToUniversalTime();
            return dtDateTime;
        }

        IMongoDatabase databaseLocation = null;

        private void SendFcmNotification(List<FcmDetails> finalList, AlertData alertObj)
        {
            WriteLogMessage("SendFcmNotification method entered", LogFile);
            //DateTime dt = Convert.ToDateTime(Trackdatetime);
            //string Time = dt.ToString("dd-MM-yyyy hh:mm tt");
            int SendStatus = 0;
            double timeZone = 0;


            string NStatus = "Normal";
            string Loc = "";

            if (string.IsNullOrEmpty(alertObj.Loc))
            {
                try
                {
                    if (databaseLocation == null)
                        databaseLocation = GetMongoLocationConnection();

                    IMongoCollection<LocationAddressData> _collection = databaseLocation.GetCollection<LocationAddressData>("locationdata");

                    var condition = Builders<LocationAddressData>.Filter;
                    var filter = condition.And(condition.Eq("latitude", Convert.ToString(alertObj.Lat)), condition.Eq("longitude", Convert.ToString(alertObj.Lon)));
                    var fields = Builders<LocationAddressData>.Projection.Include("address1").Include("address2").Include("address3").Include("address4").Exclude("_id");

                    var addr = _collection.Find<LocationAddressData>(filter).Project(fields).FirstOrDefault();

                    if (addr != null)
                    {
                        WriteLogMessage("Address is fetched ", LogFile);
                        var loc = JsonConvert.DeserializeObject<LocationAddressData>(addr.ToJson());
                        Loc = loc.Address1 + ',' + loc.Address2 + ',' + loc.Address3 + ',' + loc.Address4;
                    }


                    GC.SuppressFinalize(_collection);

                    if (string.IsNullOrEmpty(Loc) || Loc.Contains(", , ,"))
                    {
                        Loc = "";
                    }
                }
                catch (Exception ex)
                {
                    WriteLogMessage("Error occured in Fetching Location data. error message - " + ex.Message, LogFile);
                    ex = null;
                }
            }
            else
            {
                Loc = alertObj.Loc;
            }


            var json1 = JsonConvert.SerializeObject(alertObj);
            WriteLogMessage("Notification  Data Alert Data : " + json1, LogFile);

            string alerttype = GetAlertName(alertObj.AlertCode);
            DataSet ds = SH.GetimeiDetails(Connectionstring);

            var record = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("IMEI") == alertObj.IMEI).FirstOrDefault();

            if (record != null)
                timeZone = Convert.ToDouble(record.Field<string>("TimeZone"));

            DateTime dt = UnixTimeStampToDateTime(Convert.ToDouble(alertObj.TimeStamp) + timeZone * 60);


            foreach (FcmDetails item in finalList)
            {
                try
                {
                    var mydata = new
                    {
                        to = item.FcmToken,
                        data = new
                        {
                            VehicleNo = "\"" + alertObj.AssetNo + "\"",
                            AlertFor = "\"" + alerttype + "\"",
                            AlertDateTime = "\"" + dt.ToString("dd-MM-yyyy hh:mm tt") + "\"",
                            Location = "\"" + Loc + "\"",
                            Latitude = "\"" + alertObj.Lat + "\"",
                            Longitude = "\"" + alertObj.Lon + "\"",
                            NotificationType = "\"" + NStatus + "\"",
                        },
                        priority = "high",
                    };


                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(mydata);
                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                    tRequest.Method = "post";
                    tRequest.ContentType = "application/json";
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", appKey));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;
                    WriteLogMessage("Notification  Data : " + item.IMEI + " Alert Data : " + json, LogFile);

                    try
                    {
                        using (Stream dataStream = tRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);

                            using (WebResponse tResponse = tRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        String sResponseFromServer = tReader.ReadToEnd();
                                        dynamic jsonData = Newtonsoft.Json.Linq.JObject.Parse(sResponseFromServer);
                                        // string msg = jsonData.results().error;

                                        if (jsonData.success == 1)
                                        {
                                            SendStatus = 1; //success.
                                            WriteLogMessage("Notification  delivered to : " + item.IMEI + " Alert Data : " + json, LogFile);

                                        }
                                        else
                                        {
                                            SendStatus = 2; //Fail
                                            WriteLogMessage("Notification not delivered : " + item.IMEI + " Alert Data : " + json, LogFile);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLogMessage("Notification not delivered : " + item.IMEI + " Alert Data : " + json + " Exception : " + ex.Message, LogFile);
                    }
                    WriteLogMessage("Notification Sent : " + item.IMEI + " Alert Data : " + json, LogFile);

                    if (SendStatus > 0)
                    {

                        bool status = SendStatus == 1 ? true : false;

                        int tms = Convert.ToInt32(alertObj.TimeStamp);
                        DateTime receivedTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);

                        //using (ISession session = GetCassandraConnection())
                        //{
                        //    try
                        //    {
                        //        PreparedStatement prepared = session.Prepare("insert into fcmsentdetails (username, tms, sentstatus, data, receivedtime) values(?,?,?,?,?)");

                        //        BoundStatement bound = prepared.Bind(item.UserId, tms, SendStatus, json, receivedTime);
                        //        var rs = session.Execute(bound);

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        WriteErrorLogMessage("Cassandra Insertion Failed For : " + item.IMEI + " Alert Data : " + json + " Exception : " + ex.Message, LogFile);
                        //    } 
                        //}

                        if (database == null)
                            database = GetMongoConnection();


                        IMongoCollection<BsonDocument> collection = database.GetCollection<BsonDocument>("fcmsentdetails");

                        var document = new BsonDocument();
                        document.Add("username", item.UserId);
                        document.Add("tms", tms);
                        document.Add("sentstatus", status);
                        document.Add("data", json);
                        document.Add("receivedtime", receivedTime.ToString());
                        collection.InsertOne(document);

                    }

                }
                catch (Exception ex)
                {
                    WriteErrorLogMessage("Error While sending FCM Message to : " + item.IMEI + " ---- Error is: " + ex.Message, LogFile);
                    ex = null;
                }
                finally
                {
                    ds.Dispose();
                }
            }
        }


        public IMongoDatabase GetMongoLocationConnection()
        {
            int MongoPort = 0;
            string MongoServerIp = string.Empty;
            string username = string.Empty;
            string password = string.Empty;
            string KeySpace = string.Empty;
            string mongoConnectionString = string.Empty;
            IMongoDatabase database = null;
            try
            {
                MongoServerIp = ConfigurationManager.AppSettings["MongoServer"];
                MongoPort = Convert.ToInt32(ConfigurationManager.AppSettings["MongoPort"]);
                username = Convert.ToString(ConfigurationManager.AppSettings["MongoUserNameLocation"]);
                password = Convert.ToString(ConfigurationManager.AppSettings["MongoPasswordLocation"]);
                KeySpace = Convert.ToString(ConfigurationManager.AppSettings["MongoDataBaseLocation"]);
            }
            catch (Exception ex)
            {
                ex = null;
            }

            try
            {
                //Connecting to Mongo DB without authentication.
                //mongoConnectionString = "mongodb://" + MongoServerIp + ":" + MongoPort + "/"; 
                //Connecting to Mongo DB with authentication. Username and password
                mongoConnectionString = "mongodb://" + username + ":" + password + "@" + MongoServerIp + ":" + MongoPort + "/" + KeySpace;
                IMongoClient client = new MongoClient(mongoConnectionString);
                database = client.GetDatabase(KeySpace);
            }
            catch (Exception ex)
            {
                ex = null;
            }
            return database;
        }


        private static AlertData GetAlertObject(string data)
        {
            AlertData alertObj = new AlertData();

            string[] alertList = data.Split('^');

            if (alertList.Length == 10)
            {
                alertObj.AssetNo = alertList[0].Trim('(');
                alertObj.IMEI = alertList[1];
                alertObj.TimeStamp = alertList[2];
                alertObj.Lat = alertList[3];
                alertObj.Lon = alertList[4];
                alertObj.AlertCode = alertList[5];
                alertObj.MobileNo = alertList[6];
                alertObj.EmailId = alertList[7];
                alertObj.Loc = alertList[8];
                alertObj.AssetType = alertList[9].Trim(')');
            }

            return alertObj;
        }

        private void SendNotificationAlerts(string StudentName, string VehicleNo, DateTime Trackdatetime, string Location, string ImeiNo, string FCMToken, string AlertFor, string Latitide, string Longitude, string RouteNo, string StudentID, string BusStop, string GeofenceName, string Status, bool DoNotDiturb, string MobileNo)
        {
            DateTime dt = Convert.ToDateTime(Trackdatetime, CultureInfo.InvariantCulture);
            string Time = dt.ToString("dd-MM-yyyy hh:mm tt");
            sbyte SendStatus = 0;

            WriteLogMessage("SendNotificationAlerts method entered", LogFile);

            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                string NStatus = "Normal";
                var mydata = new
                {
                    to = FCMToken,
                    data = new
                    {
                        BusNo = "\"" + VehicleNo + "\"",
                        AlertFor = "\"" + AlertFor + "\"",
                        StudentName = "\"" + StudentName + "\"",
                        Title = "\"" + "School Bus Alert " + "\"",
                        TrackDateTimeTime = "\"" + Time + "\"",
                        Location = "\"" + Location + "\"",
                        Latitude = "\"" + Latitide + "\"",
                        Longitude = "\"" + Longitude + "\"",
                        NotificationType = "\"" + NStatus + "\"",
                    },
                    priority = "high",
                };


                if (DoNotDiturb == false)
                {
                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(mydata);
                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", appKey));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;

                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);

                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    dynamic jsonData = Newtonsoft.Json.Linq.JObject.Parse(sResponseFromServer);
                                    // string msg = jsonData.results().error;

                                    if (jsonData.success == 1)
                                    {
                                        SendStatus = 1; //success.
                                        WriteLogMessage("Notification  delivered to : " + MobileNo + " Response: " + sResponseFromServer, LogFile);

                                    }
                                    else
                                    {
                                        SendStatus = 2; //Fail
                                        WriteLogMessage("Notification not delivered : " + MobileNo + " Response: " + sResponseFromServer, LogFile);
                                    }
                                    UpdateNotificationSendStatus(VehicleNo, StudentID, RouteNo, Trackdatetime, BusStop, Status, GeofenceName, DoNotDiturb, Latitide, Longitude, SendStatus, Location, MobileNo);
                                }
                            }
                        }
                    }
                }
                else
                {
                    UpdateNotificationSendStatus(VehicleNo, StudentID, RouteNo, Trackdatetime, BusStop, Status, GeofenceName, DoNotDiturb, Latitide, Longitude, SendStatus, Location, MobileNo);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogMessage("Error While sending FCM Message to: " + VehicleNo + "Error is: " + ex.Message, LogFile);
                ex = null;
            }
        }

        private void GetCustomNotificationAlerts()
        {
            DataSet GNA = new DataSet();

            try
            {
                GNA = SH.Get_GeofenceNotificationAlerts(Connectionstring);

                if (GNA != null)
                {
                    if (GNA.Tables.Count > 0 && GNA.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < GNA.Tables[0].Rows.Count; i++)
                        {
                            int Slno = Convert.ToInt32(GNA.Tables[0].Rows[i]["Slno"].ToString());
                            string VehicleNo = GNA.Tables[0].Rows[i]["VehicleNo"].ToString();
                            string StudentName = GNA.Tables[0].Rows[i]["StudentName"].ToString();
                            string Trackdatetime = GNA.Tables[0].Rows[i]["TrackdateTime"].ToString();
                            string DeviceNo = GNA.Tables[0].Rows[i]["DeviceNo"].ToString();
                            string Latitide = GNA.Tables[0].Rows[i]["Latitude"].ToString();
                            string Longitude = GNA.Tables[0].Rows[i]["Longitude"].ToString();
                            string FCMToken = GNA.Tables[0].Rows[i]["FCMToken"].ToString();
                            string Location = GNA.Tables[0].Rows[i]["Location"].ToString();
                            string AlertFor = GNA.Tables[0].Rows[i]["AlertFor"].ToString();

                            WriteLogMessage("Notification Data Received For :" + VehicleNo, LogFile);

                            SendCustomNotificationAlerts(StudentName, VehicleNo, Trackdatetime, Location, DeviceNo, FCMToken, AlertFor, Latitide, Longitude);
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                WriteErrorLogMessage("Error in SendGeofenceNotificationAlerts() : " + exe.Message, LogFile);
                exe = null;
            }

        }

        private void SendCustomNotificationAlerts(string StudentName, string VehicleNo, string Trackdatetime, string Location, string ImeiNo, string FCMToken, string AlertFor, string Latitide, string Longitude)
        {
            DateTime dt = Convert.ToDateTime(Trackdatetime);
            string Time = dt.ToString("dd-MM-yyyy hh:mm tt");

            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                string NStatus = "Custom";
                var mydata = new
                {
                    to = FCMToken,
                    data = new
                    {
                        BusNo = "\"" + VehicleNo + "\"",
                        AlertFor = "\"" + AlertFor + "\"",
                        StudentName = "\"" + StudentName + "\"",
                        Title = "\"" + "School Bus Alert " + "\"",
                        TrackDateTimeTime = "\"" + Time + "\"",
                        Location = "\"" + Location + "\"",
                        Latitude = "\"" + Latitide + "\"",
                        Longitude = "\"" + Longitude + "\"",
                        NotificationType = "\"" + NStatus + "\""
                    },
                    priority = "high",
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(mydata);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", appKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                dynamic jsonData = Newtonsoft.Json.Linq.JObject.Parse(sResponseFromServer);
                                if (jsonData.success == 1)
                                {
                                    //UpdateNotificationSendStatus(VehicleNo, StudentID, RouteNo, Trackdatetime, BusStop, Status, GeofenceName);
                                    WriteLogMessage("Custom Notification sent to IMEI : " + ImeiNo, LogFile);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogMessage("Error While sending FCM Message to: " + ImeiNo + "Error is: " + ex.Message, LogFile);
                ex = null;
            }
        }

        private void UpdateNotificationSendStatus(string VehicleNo, string StudentID, string RouteNo, DateTime Trackdatetime, string BusStop, string Status, string GeofenceName, bool DoNotDiturb, string Latitide, string Longitude, sbyte SendStatus, string Location, string MobileNo)
        {
            try
            {
                int Result = SH.Insert_NotificationAlertsStatus(Connectionstring, VehicleNo, StudentID, RouteNo, Trackdatetime, BusStop, Status, GeofenceName, DoNotDiturb, Latitide, Longitude, SendStatus, Location, MobileNo);
            }
            catch (Exception ex)
            {
                ex = null;
            }
        }

        public void WriteErrorLogMessage(string strMessage, string LogFile)
        {
            string date = DateTime.Now.ToShortDateString();
            string str3 = null;
            string str4 = null;

            str4 = string.Format("{0:dd}", DateTime.Now);
            str4 = str4 + string.Format("{0:MMM}", DateTime.Now);
            str4 = str4 + "_" + string.Format("{0:HH}", DateTime.Now) + "H";
            date = date.Replace("/", ".");
            StreamWriter StreamW = null;
            //   string strFileName = "Error" + "_" + date + ".txt";
            string strFileName = "Error" + "_" + str4 + ".txt";
            //string strFilePath = LogFile;

            string strFilePath = Directory.GetCurrentDirectory();

            try
            {
                strFilePath = strFilePath + "\\ErrorLog";
                str3 = strFilePath + "\\" + strFileName;

                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }

                if (!File.Exists(str3))
                {
                    File.Create(str3).Close();
                }

                try
                {
                    StreamW = File.AppendText(str3);
                    StreamW.WriteLine(strMessage);
                    StreamW.Flush();
                }
                catch (Exception exception)
                {
                    exception = null;
                    // Console.WriteLine("File Could Not Be Created Or Written" + exception.Message);
                }
            }
            catch (Exception e)
            {
                e = null;
            }
            finally
            {
                if (StreamW != null)
                {
                    StreamW.Close();
                }
            }
        }

        public void WriteLogMessage(string strMessage, string LogFile)
        {
            string date = DateTime.Now.ToShortDateString();
            string str3 = null;
            string str4 = null;

            str4 = string.Format("{0:dd}", DateTime.Now);
            str4 = str4 + string.Format("{0:MMM}", DateTime.Now);
            str4 = str4 + "_" + string.Format("{0:HH}", DateTime.Now) + "H";
            date = date.Replace("/", ".");

            StreamWriter StreamW = null;
            string strFileName = "Log" + "_" + str4 + ".txt";
            //string strFilePath = LogFile;

            string strFilePath = Directory.GetCurrentDirectory();

            try
            {
                strFilePath = strFilePath + "\\Log";
                str3 = strFilePath + "\\" + strFileName;

                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }

                if (!File.Exists(str3))
                {
                    File.Create(str3).Close();
                }
                try
                {
                    StreamW = File.AppendText(str3);
                    StreamW.WriteLine(DateTime.Now.ToString() + " Log" + " :" + strMessage);
                    StreamW.WriteLine();
                    StreamW.Flush();
                }
                catch (Exception exception)
                {
                    exception = null;
                    // Console.WriteLine("File Could Not Be Created Or Written" + exception.Message);
                }
            }
            catch (Exception e)
            {
                e = null;
            }
            finally
            {
                if (StreamW != null)
                {
                    StreamW.Close();
                }
            }
        }

        //public ISession GetCassandraConnection()
        //{
        //    Cluster cluster = null;
        //    ISession session = null;

        //    C_ServerIp = ConfigurationManager.AppSettings["C_Server"];
        //    C_Port = Convert.ToInt32(ConfigurationManager.AppSettings["C_Port"]);
        //    C_KeySpace = ConfigurationManager.AppSettings["C_Keyspace"];
        //    C_UserName = ConfigurationManager.AppSettings["C_UserName"];
        //    C_Password = ConfigurationManager.AppSettings["C_Password"];

        //    try
        //    {
        //        var poolingOptions = new PoolingOptions();
        //        poolingOptions.SetMaxRequestsPerConnection(32768)
        //            .SetMaxConnectionsPerHost(HostDistance.Local, 8)
        //            .SetMaxConnectionsPerHost(HostDistance.Remote, 8)
        //            .SetHeartBeatInterval(60000);


        //        //cluster = Cluster.Builder().AddContactPoint(C_ServerIp).WithPort(C_Port).WithCredentials(C_UserName, C_Password).Build();

        //        cluster = Cluster.Builder()
        //           .AddContactPoint(C_ServerIp)
        //           .WithPort(C_Port)
        //           .WithCredentials(C_UserName, C_Password)
        //           .WithPoolingOptions(poolingOptions)
        //           .WithReconnectionPolicy(new ConstantReconnectionPolicy(500))
        //           .WithLoadBalancingPolicy(new RoundRobinPolicy())
        //           .Build();

        //        session = cluster.Connect(C_KeySpace);
        //    }
        //    catch (Exception ex)
        //    {
        //        // Log.Error("Unable to connect Cassandra" + ex.Message);
        //        ex = null;
        //    }

        //    return session;
        //}

        public IMongoDatabase GetMongoConnection()
        {
            IMongoDatabase database = null;

            string MongoServerIp1 = ConfigurationManager.AppSettings["MongoFirstServer"];
            int MongoPort1 = Convert.ToInt32(ConfigurationManager.AppSettings["MongoFirstPort"]);

            string MongoServerIp2 = ConfigurationManager.AppSettings["MongoSecondServer"];
            int MongoPort2 = Convert.ToInt32(ConfigurationManager.AppSettings["MongoSecondPort"]);

            string MongoServerIp3 = ConfigurationManager.AppSettings["MongoThirdServer"];
            int MongoPort3 = Convert.ToInt32(ConfigurationManager.AppSettings["MongoThirdPort"]);

            string KeySpace = Convert.ToString(ConfigurationManager.AppSettings["MongoDataBase"]);
            string username = Convert.ToString(ConfigurationManager.AppSettings["MongoUser"]);
            string password = Convert.ToString(ConfigurationManager.AppSettings["MongoPassword"]);

            string replica = MongoServerIp1 + ":" + MongoPort1; //+ "," + MongoServerIp2 + ":" + MongoPort2 + "," + MongoServerIp3 + ":" + MongoPort3;

            // string serverIpPort = "mongodb://" + replica + "/";
            string serverIpPort = "mongodb://" + username + ":" + password + "@" + replica + "/" + KeySpace;

            try
            {
                IMongoClient client = new MongoClient(serverIpPort);
                database = client.GetDatabase(KeySpace);
            }
            catch (Exception ex)
            {
                WriteErrorLogMessage("Unable to connect MongoDB -- " + ex.Message, LogFile);
                ex = null;
            }

            return database;
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            bool isSendEMail = Convert.ToBoolean(ConfigurationManager.AppSettings["isSendNotifyEmail"]);

            if (isSendEMail)
            {
                string subject = "FCM Notification EXE Stopped";
                string body = "This message is to notify that the FCM Notification EXE has been stopped. Please take the necessary action.";

                string FromMailid = ConfigurationManager.AppSettings["FromMailId"].ToString();
                string pwd = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
                string smtpServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"].ToString();

                string smtpUser = System.Configuration.ConfigurationManager.AppSettings["SMTPUser"].ToString();

                string SMTPPassword = System.Configuration.ConfigurationManager.AppSettings["SMTPPassword"].ToString();

                string ToMailid = ConfigurationManager.AppSettings["ToMails"].ToString();
                string CCMails = ConfigurationManager.AppSettings["CCMails"].ToString();
                try
                {
                    if (ToMailid != "")
                    {
                        MailMessage mailMessage = new MailMessage(FromMailid, ToMailid);
                        mailMessage.Subject = subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = body;

                        if (CCMails != "")
                        {
                            string[] arrccemail = CCMails.Split(',');
                            for (int i = 0; i < arrccemail.Count(); i++)
                            {
                                string ccmail = arrccemail[i].ToString().Trim();
                                MailAddress ccEmail = new MailAddress(ccmail);
                                if (ccEmail.Address.Length > 5)
                                {
                                    mailMessage.CC.Add(ccEmail);
                                }
                            }
                        }

                        mailMessage.Bcc.Add("noresponse.tracking@gmail.com");

                        SmtpClient smtpClient = new SmtpClient();
                        smtpClient.Port = 587;
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Host = smtpServer;
                        smtpClient.Credentials = new System.Net.NetworkCredential(smtpUser, SMTPPassword);
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(mailMessage);
                        Thread.Sleep(10000);
                    }


                }
                catch (Exception ex)
                {
                    ex = null;
                }
            }
            base.OnClosing(e);
        }
    }
}

public class LocationAddressData
{
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string Address4 { get; set; }
}
